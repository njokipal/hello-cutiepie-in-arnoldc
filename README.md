#Christmas themed Hello Cutiepie (Hello World) in arnoldC

##What is arnoldC?

arnoldC is a programming language developed by [Ihartikk](https://github.com/lhartikk/ArnoldC). It consists of Arnold Schwarzenegger's one-liners and is compiled with Ihartikk's Java based compiler into Java bytecode.

##Installation

If not installed, install Java.

`cd` into this directory and download compiler with `wget http://lhartikk.github.io/ArnoldC.jar`.

##Usage##

Once in right directory

	java -jar ArnoldC.jar hello_cutiepie.arnoldc
	java hello_cutiepie
